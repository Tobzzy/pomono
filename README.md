# Movie App

A Simple movie search App

# Technologies

- Node.js v10.x or later
- npm v5.x or later
- git v2.14.1 or later

## Installation

- Git clone repo [Pamono](https://gitlab.com/Tobzzy/Pamono)

#### Pre-requisite

```Bash
cd pamono
```

- Use the package manager [yarn](https://yarnpkg.com/) to install app

```bash
npm install or yarn install
```

- Add .env file

```
REACT_APP_API_KEY= value
```

- To start client

```bash
npm start or yarn start
```

- Client will be live on [localhost](http://localhost:3000) port 3000

# Important Choices

## Packages / Libraries

### Typescript

### styled-components

### Ant Design

## Component Structure

- Splitting component - To make component readable and reusable.
- Custom hooks - To avoid calling too many useState in the component itself.
- Using functions - To have a function that does only one specific thing.

### Author

- Toyib Olamide Ahmed
- Website - [Portfolio](https://tobzzy.github.io/)
- Email - (ahmedtoyib1@gmail.com)
- Reach out for collaboration lets build something awesome together.

## License

[MIT](https://choosealicense.com/licenses/mit/)
