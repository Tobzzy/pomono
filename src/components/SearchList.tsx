import React from "react";
import MovieState from "../state/MovieState";
import styled from "styled-components";

const Main = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
`;
const Item = styled.div`
  margin-right: 5px;
  background: #6e6d6d;
  color: #fff;
  width: 100%;
`;

export default function SearchList() {
  const { searchList } = MovieState.useContainer();
  const lastThree = searchList.slice(Math.max(searchList.length - 3, 0));

  return (
    <Main>
      <h3>previously searched:</h3>
      <div style={{ display: "flex" }}>
        {lastThree.map((item) => (
          <Item>{item}</Item>
        ))}
      </div>
    </Main>
  );
}
