import React from "react";
import { Row, Col, Card, Typography } from "antd";
import styled from "styled-components";
import MovieState from "../state/MovieState";

const MovieCard = styled(Card)`
  background-color: #6e6d6d;
  margin-bottom: 15px;
  color: #fff;
  .ant-card-head-title {
    color: #fff;
  }
  .ant-card-body {
    padding: 15px;
  }
`;
const CardImage = styled.img`
  height: 100%;
  width: 100%;
  object-fit: cover;
`;
const Title = styled(Typography)`
  color: #fff;
`;

export default function MovieList() {
  const { movies } = MovieState.useContainer();

  if (movies === undefined || typeof movies === "string") {
    return (
      <div>
        <h3>Movie not found</h3>
        <p>
          Error: <i style={{ color: "red" }}>{movies}</i>
        </p>
      </div>
    );
  }
  return (
    <Row gutter={15}>
      {movies.map((movie) => (
        <Col xs={24} sm={12} md={8} lg={8} xl={6}>
          <MovieCard
            title={movie.Title}
            hoverable={false}
            bordered
            cover={<CardImage alt={movie.Title} src={movie.Poster} />}
          >
            <Title>Type: {movie.Type}</Title>
            <Title>Year: {movie.Year}</Title>
          </MovieCard>
        </Col>
      ))}
    </Row>
  );
}
