import React from "react";
import MovieSearch from "./MovieSearch";
import styled from "styled-components";
import MovieList from "./MovieList";

const Main = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 5vw;
`;
export default function App() {
  return (
    <Main>
      <MovieSearch />
      <MovieList />
    </Main>
  );
}
