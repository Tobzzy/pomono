import React, { useState } from "react";
import { Input, Button } from "antd";
import styled from "styled-components";
import MovieState from "../state/MovieState";
import SearchList from "./SearchList";
import getMovie from "../util/getMovies";

const Main = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 5vh;
`;

export default function MovieSearch() {
  const [value, setValue] = useState("");
  const { setMovies, searchList, setSearchList } = MovieState.useContainer();

  const handleSearch = async () => {
    setSearchList([...searchList, value]);
    const movies = await getMovie(value);
    setMovies(movies);
  };
  return (
    <Main>
      <div>
        <Input
          style={{ width: "300px", marginRight: "10px" }}
          type="search"
          bordered
          placeholder="search for a movie..."
          onChange={(e) => setValue(e.target.value)}
        />
        <Button onClick={handleSearch}>Search</Button>
      </div>
      {searchList.length !== 0 && <SearchList />}
    </Main>
  );
}
