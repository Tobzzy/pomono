import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import "antd/dist/antd.css";
import MovieState from "./state/MovieState";

ReactDOM.render(
  <React.StrictMode>
    <MovieState.Provider>
      <App />
    </MovieState.Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
