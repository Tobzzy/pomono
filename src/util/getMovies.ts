export default function getMovie(value: string) {
  const { REACT_APP_API_KEY } = process.env;
  return fetch(
    `https://www.omdbapi.com/?s=${value}&apikey=${REACT_APP_API_KEY}`
  )
    .then((response) => response.json())
    .then((res) => {
      if (res.Response === "True") {
        return res.Search;
      } else {
        return res.Error;
      }
    });
}
