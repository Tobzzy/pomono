import { useState } from "react";
import { createContainer } from "unstated-next";
import usePersistedState from "../hooks/usePersistedState";

interface MovieProps {
  Poster: string;
  Title: string;
  Type: string;
  Year: string;
  imdbID: string;
}

const useMovieState = () => {
  const [movies, setMovies] = useState<MovieProps[]>([]);
  const [searchList, setSearchList] = usePersistedState<Array<string>>(
    "SearchList",
    []
  );

  return {
    movies,
    setMovies,
    searchList,
    setSearchList,
  };
};

export default createContainer(useMovieState);
